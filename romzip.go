package main

import (
	"archive/zip"
	"fmt"
	"github.com/uwedeportivo/torrentzip"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
)

func main() {
	for _, path := range os.Args[1:] {
		err := filepath.Walk(path, visit)
		if err != nil {
			log.Fatal(err)
		}
	}
}

func visit(path string, fileInfo os.FileInfo, err error) error {
	if err != nil {
		return err
	}
	if fileInfo.IsDir() || filepath.Ext(path) == ".zip" {
		return nil
	}

	fileName := filepath.Base(path)
	targetDir := filepath.Dir(path)
	zipPath := filepath.Join(targetDir, baseName(path)+".zip")

	if _, err := os.Stat(zipPath); err == nil || !os.IsNotExist(err) {
		zipReader, err := zip.OpenReader(zipPath)
		if err != nil {
			return err
		}
		zippedFile := getFileFromZip(zipReader.File, fileName)
		if zippedFile != nil {
			zipReader.Close()
			if zippedFile.FileHeader.UncompressedSize64 != uint64(fileInfo.Size()) {
				fmt.Println("SKIPPING:", path, "(files differ)")
				return nil
			}
		} else {
			workingZipPath, err := addFileToZip(path, fileName, zipReader.Reader)
			zipReader.Close()
			if err != nil {
				return err
			}
			if err = os.Rename(workingZipPath, zipPath); err != nil {
				return err
			}
		}
	} else {
		if err = createZipFromFile(path, fileName, zipPath); err != nil {
			return err
		}
	}

	err = os.Remove(path)
	if err != nil {
		return err
	}

	fmt.Println("PROCESSED:", path)
	return nil
}

func addFileToZip(filePath string, fileName string, zipReader zip.Reader) (string, error) {
	fileReader, err := os.Open(filePath)
	if err != nil {
		return "", err
	}
	defer fileReader.Close()

	workingZip, err := ioutil.TempFile(os.TempDir(), "romzip-")
	if err != nil {
		return "", err
	}
	defer workingZip.Close()

	workingZipWriter, err := torrentzip.NewWriter(workingZip)
	if err != nil {
		return "", err
	}
	defer workingZipWriter.Close()

	for _, f := range zipReader.File {
		src, err := f.Open()
		if err != nil {
			return "", err
		}
		if addReaderToZipWriter(f.FileHeader.Name, src, workingZipWriter); err != nil {
			return "", err
		}
	}

	if err = addReaderToZipWriter(fileName, fileReader, workingZipWriter); err != nil {
		return "", err
	}

	return workingZip.Name(), nil
}

func addReaderToZipWriter(fileName string, reader io.Reader, zipWriter *torrentzip.Writer) error {
	fileWriter, err := zipWriter.Create(fileName)
	if err != nil {
		return err
	}

	_, err = io.Copy(fileWriter, reader)
	return err
}

func createZipFromFile(filePath string, fileName string, zipPath string) error {
	fileReader, err := os.Open(filePath)
	if err != nil {
		return err
	}
	defer fileReader.Close()

	zipFile, err := os.Create(zipPath)
	if err != nil {
		return err
	}
	defer zipFile.Close()

	zipWriter, err := torrentzip.NewWriter(zipFile)
	if err != nil {
		return err
	}
	file, err := zipWriter.Create(fileName)
	if err != nil {
		return err
	}

	if _, err = io.Copy(file, fileReader); err != nil {
		return err
	}
	return zipWriter.Close()
}

func baseName(filePath string) string {
	fileName := filepath.Base(filePath)
	return strings.TrimSuffix(fileName, filepath.Ext(fileName))
}

func getFileFromZip(files []*zip.File, path string) *zip.File {
	for _, f := range files {
		if f.FileHeader.Name == path {
			return f
		}
	}
	return nil
}
